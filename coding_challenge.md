Coding Challenge
===========================

If you don't have code to share, you can work on our coding challenge described
below. 

Please organize, design, test, document and deploy your code as if it were
going into production, then send us a link to the hosted repository (e.g.
Github, Bitbucket...).

Functional spec
---------------

The UX/UI is totally up to you. If you like, get creative and add additional
features a user might find useful!

Prototype **one** of the following projects:

1. Meal Plan Generator


### Meal Plan Generator

Create a service that accepts the necessary information and generates meal plans. It
should provide an abstraction between our service and third party meal plan generating API.
If one of the services goes down, you should be able to still provide data for current clients that mealplan was already generated earlier
without affecting your customers. (It could be saved in database, or persisted in any other storage). Generation service should be paused until functionallity restored back on.

For a frontend task, you can implement API directly, and provide only frontend layer of Meal Plan Generation interface. 
The main functionallity should be to generate, view or edit your mealplan.

Third party API providers:

* [spoonacular](https://spoonacular.com/) - [Spoonacular Documentation](https://spoonacular.com/food-api/docs)

All listed services are free to try and are pretty painless to sign up for, so
please register your own test accounts on each.


Technical spec
--------------

The architecture will be split between a back-end and a web front-end, for
instance providing a JSON in/out RESTful API. Feel free to use any other
technologies provided that the general client/service architecture is
respected.

Choose **one** of the following technical tracks that best suits your skillset:

1. **Full-stack**: include both front-end and back-end.
2. **Back-end track**: include a minimal front-end (e.g. a static view or API
   docs). Write, document and test your API as if it will be used by other
   services.
3. **Front-end track**: include a minimal back-end, or use the data service
   directly. Focus on making the interface as polished as possible.

### Back-end

We believe there is no one-size-fits-all technology. Good engineering is about
using the right tool for the right job, and constantly learning about them.
Therefore, feel free to mention in your `README` how much experience you have
with the technical stack you choose, we will take note of that when reviewing
your challenge.

Here are some technologies we are more familiar with:

* PHP (Laraver, Symfony)
* JavaScript (Vanilla, Vue.js, React, React native, Node.js)
* Java
* Go


You are also free to use any web framework. If you choose to use a framework
that results in boilerplate code in the repository, please detail in your
README which code was written by you (as opposed to generated code).

### Front-end

The front-end should ideally be a single page app with a single `index.html`
linking to external JS/CSS/etc. You may take this opportunity to demonstrate
your CSS3 or HTML5 knowledge.

Host it!
--------

When you�re done, host it somewhere (e.g. on Amazon EC2, Heroku, Google AppEngine, etc.).

How will we review?
-------------------

[Guidelines can be found here](https://github.com/uber/coding-challenge-tools/blob/master/README.md)
